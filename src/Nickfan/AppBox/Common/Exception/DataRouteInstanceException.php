<?php

namespace Nickfan\AppBox\Common\Exception;

class DataRouteInstanceException extends \Exception implements AppBoxExceptionInterface {
}
