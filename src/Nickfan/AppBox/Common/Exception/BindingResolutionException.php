<?php

namespace Nickfan\AppBox\Common\Exception;

class BindingResolutionException extends \Exception implements AppBoxExceptionInterface {
}
