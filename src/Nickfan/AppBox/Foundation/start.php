<?php
/**
 * Description
 *
 * @project appbox
 * @package
 * @author nickfan<nickfan81@gmail.com>
 * @link http://www.axiong.me
 * @version $Id$
 * @lastmodified: 2014-06-19 11:47
 *
 */

use Nickfan\AppBox\Common\Usercache\ApcUsercache;
use Nickfan\AppBox\Common\Usercache\YacUsercache;
use Nickfan\AppBox\Common\Usercache\NullUsercache;
use Nickfan\AppBox\Config\DataRouteConf;
use Nickfan\AppBox\Config\Repository as Config;
use Nickfan\AppBox\Foundation\AliasLoader;
use Nickfan\AppBox\Instance\DataRouteInstance;
use Nickfan\AppBox\Support\Facades\Facade;

/*
|--------------------------------------------------------------------------
| Bind The Application In The Container
|--------------------------------------------------------------------------
|
| This may look strange, but we actually want to bind the app into itself
| in case we need to Facade test an application. This will allow us to
| resolve the "app" key out of this container for this app's facade.
|
*/

$app->instance('app', $app);

/*
|--------------------------------------------------------------------------
| Load The Illuminate Facades
|--------------------------------------------------------------------------
|
| The facades provide a terser static interface over the various parts
| of the application, allowing their methods to be accessed through
| a mixtures of magic methods and facade derivatives. It's slick.
|
*/

Facade::clearResolvedInstances();

Facade::setFacadeApplication($app);

/*
|--------------------------------------------------------------------------
| Register Facade Aliases To Full Classes
|--------------------------------------------------------------------------
|
| By default, we use short keys in the container for each of the core
| pieces of the framework. Here we will register the aliases for a
| list of all of the fully qualified class names making DI easy.
|
*/

$app->registerCoreContainerAliases();
$userCacheObject = null;
if(extension_loaded('apc')){
    $userCacheObject = new ApcUsercache;
}elseif(extension_loaded('yac')){
    $userCacheObject = new YacUsercache;
}else{
    $userCacheObject = new NullUsercache;
}
$app->instance('usercache', $userCacheObject);
$app->instance('config', $config = new Config($userCacheObject, $app['path.storage'] . '/conf'));
$app->instance('datarouteconf', $routeconf = new DataRouteConf($userCacheObject, $app['path.storage'] . '/etc/local'));
$app->instance('datarouteinstance', $routeinstance = DataRouteInstance::getInstance($routeconf));

/*
|--------------------------------------------------------------------------
| Set The Default Timezone
|--------------------------------------------------------------------------
|
| Here we will set the default timezone for PHP. PHP is notoriously mean
| if the timezone is not explicitly set. This will be used by each of
| the PHP date and date-time functions throughout the application.
|
*/

$config = $app['config']['app'];

date_default_timezone_set($config['timezone']);

/*
|--------------------------------------------------------------------------
| Register The Alias Loader
|--------------------------------------------------------------------------
|
| The alias loader is responsible for lazy loading the class aliases setup
| for the application. We will only register it if the "config" service
| is bound in the application since it contains the alias definitions.
|
*/

$aliases = $config['aliases'];

AliasLoader::getInstance($aliases)->register();
