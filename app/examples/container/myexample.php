<?php
/**
 * Description
 *
 * @project appbox
 * @package
 * @author nickfan<nickfan81@gmail.com>
 * @link http://www.axiong.me
 * @version $Id$
 * @lastmodified: 2014-06-05 17:45
 *
 */

require_once __DIR__.'/../../bootstrap/bootstrap.php';

echo \Nickfan\AppBox\Config\Repository::getVersion();
